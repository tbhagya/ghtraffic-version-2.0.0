package nz.ac.massey.ghtraffic.scripts.extractor;

import java.util.Iterator;

/**
 * Factory for generating Http transactions
 * @author thilini bhagya
 */

public interface HttpTransactionFactory {
	Iterator getTransactions() throws Exception;
}
