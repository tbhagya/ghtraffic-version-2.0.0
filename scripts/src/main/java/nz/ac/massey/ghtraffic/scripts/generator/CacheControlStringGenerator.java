package nz.ac.massey.ghtraffic.scripts.generator;

/**
 * generates strings for Cache-Control header
 * @author thilini bhagya
 */
public class CacheControlStringGenerator {
    public static final String PRIVATE = "private, max-age=60";
}
