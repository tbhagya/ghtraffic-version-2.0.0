package nz.ac.massey.ghtraffic.scripts.standards;

/**
 * Lists property names for GHTraffic
 * @author thilini bhagya
 */
public interface GHTrafficProperties {
    public static final String TYPE = "type";
    public static final String SOURCE = "source";
    public static final String PROCESSOR = "processor";
    public static final String TIMESTAMP = "timestamp";
}
