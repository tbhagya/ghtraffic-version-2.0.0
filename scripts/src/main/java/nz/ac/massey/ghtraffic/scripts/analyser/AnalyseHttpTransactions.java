package nz.ac.massey.ghtraffic.scripts.analyser;
import nz.ac.massey.ghtraffic.scripts.Logging;
import org.apache.log4j.Logger;
import java.io.*;


/**
 * Script to analyse different metrics of the dataset
 * @author thilini bhagya
 */
public class AnalyseHttpTransactions {
    static Logger LOGGER = Logging.getLogger(AnalyseHttpTransactions.class);

    public static void main(String args[]) throws Exception {
        File directory = new File("../paper/Metrics");
        directory.mkdir();

        LOGGER.info("Rendering output to latex");

        //table with statistics of transactions by methods
        File latex = new File(directory, "transactions-by-methods.tex");
        try (PrintStream out = new PrintStream(new FileOutputStream(latex))) {
            out.println("\\begin{table}[]");
            out.println("\\centering");
            out.println("\\caption{Transactions per http methods}");
            out.println("\\label{table:one}");
            out.println("\\begin{tabular}{llll} \\\\");
            out.println(" & S & M & L \\\\");
            out.println("POST   &" + countWords("\"Method\":\"POST\"", "DataSet/Small/sdata.json") + "   &" + countWords("\"Method\":\"POST\"", "DataSet/Medium/mdata.json") + "  &" + countWords("\"Method\":\"POST\"", "DataSet/Large/ldata.json") + "   \\\\");
            out.println("GET   &" + countWords("\"Method\":\"GET\"", "DataSet/Small/sdata.json") + "   &" + countWords("\"Method\":\"GET\"", "DataSet/Medium/mdata.json") + "  &" + countWords("\"Method\":\"GET\"", "DataSet/Large/ldata.json") + "   \\\\");
            out.println("PATCH   &" + countWords("\"Method\":\"PATCH\"", "DataSet/Small/sdata.json") + "   &" + countWords("\"Method\":\"PATCH\"", "DataSet/Medium/mdata.json") + "  &" + countWords("\"Method\":\"PATCH\"", "DataSet/Large/ldata.json") + "   \\\\");
            out.println("DELETE   &" + countWords("\"Method\":\"DELETE\"", "DataSet/Small/sdata.json") + "   &" + countWords("\"Method\":\"DELETE\"", "DataSet/Medium/mdata.json") + "  &" + countWords("\"Method\":\"DELETE\"", "DataSet/Large/ldata.json") + "   \\\\");
            out.println("PUT   &" + countWords("\"Method\":\"PUT\"", "DataSet/Small/sdata.json") + "   &" + countWords("\"Method\":\"PUT\"", "DataSet/Medium/mdata.json") + "  &" + countWords("\"Method\":\"PUT\"", "DataSet/Large/ldata.json") + "   \\\\");
            out.println("HEAD   &" + countWords("\"Method\":\"HEAD\"", "DataSet/Small/sdata.json") + "   &" + countWords("\"Method\":\"HEAD\"", "DataSet/Medium/mdata.json") + "  &" + countWords("\"Method\":\"HEAD\"", "DataSet/Large/ldata.json") + "   \\\\");
            out.println("\\end{tabular}");
            out.println("\\end{table}");
        }
        LOGGER.info("Latex table with results created at " + latex.getAbsolutePath());

        //table with statistics of transactions by status code
        latex = new File(directory, "transactions-by-statuscode.tex");
        try (PrintStream out = new PrintStream(new FileOutputStream(latex))) {
            out.println("\\begin{table}[]");
            out.println("\\centering");
            out.println("\\caption{Transactions per response codes}");
            out.println("\\label{table:two}");
            out.println("\\begin{tabular}{llll} \\\\");
            out.println(" & S & M & L \\\\");
            out.println("201   &" + countWords("Status-Code\":201", "DataSet/Small/sdata.json") + "   &" + countWords("Status-Code\":201", "DataSet/Medium/mdata.json") + "  &" + countWords("Status-Code\":201", "DataSet/Large/ldata.json") + "   \\\\");
            out.println("204   &" + countWords("Status-Code\":204", "DataSet/Small/sdata.json") + "   &" + countWords("Status-Code\":204", "DataSet/Medium/mdata.json") + "  &" + countWords("Status-Code\":204", "DataSet/Large/ldata.json") + "   \\\\");
            out.println("200   &" + countWords("Status-Code\":200", "DataSet/Small/sdata.json") + "   &" + countWords("Status-Code\":200", "DataSet/Medium/mdata.json") + "  &" + countWords("Status-Code\":200", "DataSet/Large/ldata.json") + "   \\\\");
            out.println("400   &" + countWords("Status-Code\":400", "DataSet/Small/sdata.json") + "   &" + countWords("Status-Code\":400", "DataSet/Medium/mdata.json") + "  &" + countWords("Status-Code\":400", "DataSet/Large/ldata.json") + "   \\\\");
            out.println("401   &" + countWords("Status-Code\":401", "DataSet/Small/sdata.json") + "   &" + countWords("Status-Code\":401", "DataSet/Medium/mdata.json") + "  &" + countWords("Status-Code\":401", "DataSet/Large/ldata.json") + "   \\\\");
            out.println("404   &" + countWords("Status-Code\":404", "DataSet/Small/sdata.json") + "   &" + countWords("Status-Code\":404", "DataSet/Medium/mdata.json") + "  &" + countWords("Status-Code\":404", "DataSet/Large/ldata.json") + "   \\\\");
            out.println("422   &" + countWords("Status-Code\":422", "DataSet/Small/sdata.json") + "   &" + countWords("Status-Code\":422", "DataSet/Medium/mdata.json") + "  &" + countWords("Status-Code\":422", "DataSet/Large/ldata.json") + "   \\\\");
            out.println("500   &" + countWords("Status-Code\":500", "DataSet/Small/sdata.json") + "   &" + countWords("Status-Code\":500", "DataSet/Medium/mdata.json") + "  &" + countWords("Status-Code\":500", "DataSet/Large/ldata.json") + "   \\\\");
            out.println("\\end{tabular}");
            out.println("\\end{table}");
        }
        LOGGER.info("Latex table with results created at " + latex.getAbsolutePath());

        //table with statistics of transactions by type
        latex = new File(directory, "transactions-by-type.tex");
        try (PrintStream out = new PrintStream(new FileOutputStream(latex))) {
            out.println("\\begin{table}[]");
            out.println("\\centering");
            out.println("\\caption{Transactions per type}");
            out.println("\\label{table:three}");
            out.println("\\begin{tabular}{llll} \\\\");
            out.println(" & S & M & L \\\\");
            out.println("Real-world   &" + countWords("real-world", "DataSet/Small/sdata.json") + "   &" + countWords("real-world", "DataSet/Medium/mdata.json") + "  &" + countWords("real-world", "DataSet/Large/ldata.json") + "   \\\\");
            out.println("Synthetic   &" + countWords("synthetic", "DataSet/Small/sdata.json") + "   &" + countWords("synthetic", "DataSet/Medium/mdata.json") + "  &" + countWords("synthetic", "DataSet/Large/ldata.json") + "   \\\\");
            out.println("\\end{tabular}");
            out.println("\\end{table}");


        }
        LOGGER.info("Latex table with results created at " + latex.getAbsolutePath());
        
    }

        // count total number of elements in the dataset

    private static int countWords(String word, String fileName)  {
        int counter = 0;
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            for (String line; (line = reader.readLine()) != null; ) {
                for (int i = 0; (i = line.indexOf(word, i)) != -1; i += word.length()) {
                    counter++;
                }
            }
        } catch (Exception x) {
            LOGGER.warn("Exception writing details to log ", x);
        }
        return counter;
    }
}








