package nz.ac.massey.ghtraffic.scripts.generator;


/**
 * generates strings for Access-Control-Allow-Origin header
 * @author thilini bhagya
 */

public class OriginStringGenerator {
    public static final String ANY_ORIGIN = "*";
}
