package nz.ac.massey.ghtraffic.scripts.standards;

import java.util.Properties;

/**
 * This class represents elements of a GHTraffic HTTP transaction
 * @author thilini bhagya
 */
public class HttpTransaction {
    private HttpRequest request = null;
    private HttpResponse response = null;
    private Properties metaData = null;
    private long timestamp = 0;

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public HttpRequest getRequest(){return request;}

    public void setRequest(HttpRequest request) {
        this.request = request;
    }

    public HttpResponse getResponse() {
        return response;
    }

    public void setResponse(HttpResponse response) {
        this.response = response;
    }

    public Properties getMetaData() {
        return metaData;
    }

    public void setMetaData(Properties metaData) {
        this.metaData = metaData;
    }
}
