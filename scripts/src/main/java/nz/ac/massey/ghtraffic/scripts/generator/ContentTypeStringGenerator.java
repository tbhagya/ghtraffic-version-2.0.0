package nz.ac.massey.ghtraffic.scripts.generator;

/**
 * generates content-type strings for headers
 * @author thilini bhagya
 */
public class ContentTypeStringGenerator {
    public static final String JSON = "application/json; charset=utf-8";
    public static final String GITHUB_MEDIA_TYPE = "github.v3; format=json";
}
