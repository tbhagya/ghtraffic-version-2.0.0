package nz.ac.massey.ghtraffic.scripts.generator;

/**
 * generates strings for X-OAuth-Scopes header
 * @author thilini bhagya
 */
public class OAuthScopesStringGenerator {
    public static final String PUBLIC = "public_repo";
    public static final String PRIVATE = "private_repo";
    public static final String ANY = "repo";
}
