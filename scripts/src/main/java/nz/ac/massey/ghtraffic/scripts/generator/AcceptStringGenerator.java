package nz.ac.massey.ghtraffic.scripts.generator;

/**
 * generate strings for Accept header
 * @author thilini bhagya
 */
public class AcceptStringGenerator {
    public static final String anyMIMEtype = "*/*";
}
