package nz.ac.massey.ghtraffic.scripts.generator;


import java.util.*;

/**
 * generate string for User-agent header
 * @author thilini bhagya
 */

public class UserAgentStringGenerator {

    public static final List<String> agents = Arrays.asList("Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 1.0.3705)",
            "Mozilla/5.0 (Macintosh; U; Intel Mac OS X; en) AppleWebKit/418.9 (KHTML, like Gecko) Safari/419.3",
            "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9) Gecko/2008052906 Firefox/3.0",
            "Opera/9.27 (Windows NT 5.1; U; en)",
            "Opera/9.50 (J2ME/MIDP; Opera Mini/4.1.11320/534; U; en)");




}
