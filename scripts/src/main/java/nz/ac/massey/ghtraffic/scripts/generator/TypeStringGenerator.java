package nz.ac.massey.ghtraffic.scripts.generator;

/**
 * generate string for type parameter of GHTraffic properties
 * @author thilini bhagya
 */
public class TypeStringGenerator {
    public static final String SYNTHETIC = "synthetic";
    public static final String REAL_WORLD = "real-world";
}
