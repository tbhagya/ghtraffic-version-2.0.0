package nz.ac.massey.ghtraffic.scripts.extractor;

import com.google.common.base.Predicate;
import nz.ac.massey.ghtraffic.scripts.standards.HttpTransaction;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

/**
 * Specify filters for creating Small (S), Medium (M) and Large (L) editions of the dataset.
 * @author thilini bhagya
 */
public class Filters {

    public static Predicate<HttpTransaction> sFilter() throws IOException {
        Properties prop = new Properties();
        prop.load(new FileInputStream("config.properties"));
        return (HttpTransaction t) -> (t.getRequest().getRequestUri().contains("/"+prop.getProperty("s_owner")+"/"+prop.getProperty("s_repo")+"/"));
    }

    public static Predicate<HttpTransaction> mFilter() throws IOException{
        Properties prop = new Properties();
        prop.load(new FileInputStream("config.properties"));
        return (HttpTransaction t) -> (t.getRequest().getRequestUri().contains("/"+prop.getProperty("m_owner")+"/"+prop.getProperty("m_repo")+"/"));
    }

    public static Predicate<HttpTransaction> lFilter() throws IOException{
        Properties prop = new Properties();
        prop.load(new FileInputStream("config.properties"));
        return (HttpTransaction t) -> ((t.getRequest().getRequestUri().contains("/"+prop.getProperty("l_one_owner")+"/"+prop.getProperty("l_one_repo")+"/"))||
                (t.getRequest().getRequestUri().contains("/"+prop.getProperty("l_two_owner")+"/"+prop.getProperty("l_two_repo")+"/"))||
                (t.getRequest().getRequestUri().contains("/"+prop.getProperty("l_three_owner")+"/"+prop.getProperty("l_three_repo")+"/"))||
                (t.getRequest().getRequestUri().contains("/"+prop.getProperty("l_four_owner")+"/"+prop.getProperty("l_four_repo")+"/"))||
                (t.getRequest().getRequestUri().contains("/"+prop.getProperty("l_five_owner")+"/"+prop.getProperty("l_five_repo")+"/"))||
                (t.getRequest().getRequestUri().contains("/"+prop.getProperty("l_six_owner")+"/"+prop.getProperty("l_six_repo")+"/"))||
                (t.getRequest().getRequestUri().contains("/"+prop.getProperty("l_seven_owner")+"/"+prop.getProperty("l_seven_repo")+"/"))||
                (t.getRequest().getRequestUri().contains("/"+prop.getProperty("l_eight_owner")+"/"+prop.getProperty("l_eight_repo")+"/"))
        );
    }

}
