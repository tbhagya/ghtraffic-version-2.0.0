package nz.ac.massey.ghtraffic.scripts.standards;

/**
 * This class assigns key-value pair for multiple type parameters
 * @author thilini bhagya
 */
public class KeyValuePair<K, V>   {

    private K key;
    private V value;

    public KeyValuePair(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public K getKey()	{ return key; }
    public V getValue() { return value; }

}
