package nz.ac.massey.ghtraffic.scripts.exporter;

/**
 *
 * Filter out relevant transactions from all the factories
 * @author thilini bhagya
 */
import nz.ac.massey.ghtraffic.scripts.standards.*;
import java.io.*;
import java.util.Iterator;
import com.google.common.base.Predicate;
import nz.ac.massey.ghtraffic.scripts.Logging;
import nz.ac.massey.ghtraffic.scripts.extractor.*;
import nz.ac.massey.ghtraffic.scripts.generator.*;
import org.apache.log4j.Logger;

public class JSONWriter {
    static Logger LOGGER = Logging.getLogger(JSONWriter.class);

    public void createDatafiles(Writer out, Predicate<HttpTransaction> filter) throws Exception {

        HttpTransactionFactory[] factories = new HttpTransactionFactory[]{
                new GHTorrentTransactionFactoryForIssueCreation(),
                new GHTorrentTransactionFactoryForIssueClosing(),
                new CreateGETTransactionsForIssueListing(),
                new CreateHEADTransactionsForGettingHeaderInfo(),
                new CreatePUTTransactionsForIssueLocking(),
                new CreateDELETETransactionsForIssueUnlocking(),
                new CreatePOSTTransactionsWithBadlyFormattedURI(),
                new CreatePOSTTransactionsWithoutAuthentication(),
                new CreatePOSTTransactionsWithoutPayload(),
                new CreatePOSTTransactionsWithInvalidJSON(),
                new CreatePATCHTransactionsWithBadlyFormattedURI(),
                new CreatePATCHTransactionsWithoutAuthentication(),
                new CreatePATCHTransactionsWithoutPayload(),
                new CreatePATCHTransactionsWithInvalidJSON(),
                new CreatePATCHTransactionsBeforeIssueCreation(),
                new CreateGETTransactionsBeforeIssueCreation(),
                new CreateGETTransactionsWithBadlyFormattedURI(),
                new CreateGETTransactionsWithServerError(),
                new CreateHEADTransactionsWithBadlyFormattedURI(),
                new CreateHEADTransactionsBeforeIssueCreation(),
                new CreatePUTTransactionsWithBadlyFormattedURI(),
                new CreatePUTTransactionsWithoutAuthentication(),
                new CreatePUTTransactionsBeforeIssueCreation(),
                new CreateDELETETransactionsWithBadlyFormattedURI(),
                new CreateDELETETransactionsWithoutAuthentication(),
                new CreateDELETETransactionsBeforeIssueCreation()

        };


       for (HttpTransactionFactory factory : factories) {
            Iterator<HttpTransaction> transactions = factory.getTransactions();
            while (transactions.hasNext()) {
                HttpTransaction transaction = transactions.next();
                if (filter.test(transaction)) {
                    LOGGER.info("Transaction is matched with the filter");
                    out.append(new GHTorrentTransactionToJSONExporter().getExporter(transaction));
                    LOGGER.info("Writing to the file");
                    out.append(",");
                    out.flush();
                }
                else {
                    LOGGER.info("Transaction is not matched with the filter, skipping it");
                }

            }
        }


    }
}






