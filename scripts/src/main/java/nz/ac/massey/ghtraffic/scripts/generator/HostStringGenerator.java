package nz.ac.massey.ghtraffic.scripts.generator;

/**
 * generate string values for Host header
 * @author thilini bhagya
 */
public class HostStringGenerator {

    public static final String HOST = "api.github.com";
}
