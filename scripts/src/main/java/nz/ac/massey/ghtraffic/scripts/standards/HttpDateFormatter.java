package nz.ac.massey.ghtraffic.scripts.standards;

import java.text.ParseException;
import java.util.*;
import java.text.SimpleDateFormat;
import java.util.concurrent.ThreadLocalRandom;
/**
 * This class converts dates into HTTP date format (RFC 822/1123)
 * @see <a href="https://www.ietf.org/rfc/rfc2616.txt">RFC2616 (HTTP/1.1)</a> , section 3.3
 * @author thilini bhagya
 */


public class HttpDateFormatter extends ThreadLocal<SimpleDateFormat> {

    protected SimpleDateFormat initialValue() {
            // Formats into HTTP date format
            SimpleDateFormat f = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.US);
            f.setTimeZone(TimeZone.getTimeZone("GMT"));
            return f;
        }

        private static final HttpDateFormatter FORMATTER = new HttpDateFormatter();

        public static String getDate(String date) throws ParseException {
            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

            return FORMATTER.get().format(sdf.parse(date));
        }

    public static String getCurrentDate(){
        Date currentTime = new Date(System.currentTimeMillis());

        final SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        return sdf.format(currentTime);
    }
    public static String getPastDate(String date)throws ParseException{
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

        Calendar cal = Calendar.getInstance();
        cal.setTime(sdf.parse(date));

        int randomNum = ThreadLocalRandom.current().nextInt(100, 360 + 1);

        cal.add(Calendar.DAY_OF_YEAR, -randomNum);
        cal.add(Calendar.HOUR, -randomNum);
        cal.add(Calendar.MINUTE, -randomNum);
        cal.add(Calendar.SECOND, -randomNum);
        Date previousDate = cal.getTime();

        return (FORMATTER.get().format(previousDate));
    }

    public static String getAfterDate(String date)throws ParseException{
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

        Calendar cal = Calendar.getInstance();
        cal.setTime(sdf.parse(date));

        int randomNum = ThreadLocalRandom.current().nextInt(100, 400 + 1);

        cal.add(Calendar.DAY_OF_YEAR, +randomNum);
        cal.add(Calendar.HOUR, +randomNum);
        cal.add(Calendar.MINUTE, +randomNum);
        cal.add(Calendar.SECOND, +randomNum);
        Date previousDate = cal.getTime();

        return (FORMATTER.get().format(previousDate));
    }

}
