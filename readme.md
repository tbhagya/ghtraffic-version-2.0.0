## Introduction
This is the latest version (**version 2.0.0**) of the GHTraffic project. The main aim is to model a variety of transaction sequences to reflect more complex service behaviour. 

This version consists of two different editions: Small (S) and Large (L) where the records were created by selecting the same repositories as the original Small and Large datasets. The newest S dataset contains records from [google/guava](https://github.com/google/guava) repository. The L dataset contains records from eight repositories (i.e.,[twbs/bootstrap](https://github.com/twbs/bootstrap), [symfony/symfony](https://github.com/symfony/symfony), [docker/docker](https://github.com/docker/docker), [Homebrew/homebrew](https://github.com/Homebrew/homebrew), [rust-lang/rust](https://github.com/rust-lang/rust), [kubernetes/kubernetes](https://github.com/kubernetes/kubernetes), and [angular/angular.js](https://github.com/angular/angular.js)).

The entire data generation process is quite similar to the original GHTraffic design. But it incorporates minor changes to the process of synthetic data generation where it uses a random date after successfully posting a resource to make up the request and response for all of the HTTP methods. It also adds yet another subset of unsuccessful transactions by stipulating requests before resource creation is successful. 

This results in a far more dynamic series of transactions to named resources.

## Accessing Dataset

The dataset can be downloaded using: [S](https://zenodo.org/record/4007589/files/ghtraffic-S-2.0.0.zip) and [L](https://zenodo.org/record/4007589/files/ghtraffic-L-2.0.0.zip) 

## Using Script

You can use the given scripts to generate variants of the dataset according to your needs. You need to follow the same procedure as in GHTraffic to generate the dataset by using this specific script.

Find the GHTorrent repository from [here](https://bitbucket.org/tbhagya/ghtraffic/).

## Sample Records

There are sample records for each operation type in the [sample-records](https://bitbucket.org/tbhagya/ghtraffic-version-2.0.0/src/master/sample-records/) folder.